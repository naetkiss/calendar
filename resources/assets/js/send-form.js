$(function () {
    $("#form-ajax").on("submit", function (e) {
        e.preventDefault();
        let action = $(this).attr('action');
        let form = new FormData(this);
        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            url: action,
            data: form,
            success: function(response){
            }
        });
    })

});
