@extends('layout.base')

@section('content')
    <h2 xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">Календарь на {{$calendar->getYearNumber()}}  год.</h2>
    <div class="calendar" >
    @foreach( $calendar->getYear(2017) as $month )
            @if(($month->getNumber()-1)%4==0)
                <span><br></span>
            @endif
                <div class="month" >

                   <div class="monthName">{{ $month->getName() }}</div>
                <table>
            @foreach( $month->getWeek() as $week)
                <tr>
               @foreach($week as $day)
                   <td>
                   @if($day!=null)
                       <div class="@if($day->getIsWeekend()==1) weekend @else noWeekend @endif">
                           @if($day->getFile())
                               <a href="{{  $day->getFile() }}">
                                   {{$day->getDayNumber()}}
                               </a>
                           @else
                               <span>
                                   {{$day->getDayNumber()}}
                               </span>
                           @endif
                       </div>
                   @endif
                   </td>
                @endforeach
                </tr>
                    @if($week%6!=0)
                    <tr>
                        @for($i=0;$i<=6;$i++)

                        @endfor
                    </tr>
                    @endif
            @endforeach
                    </table>
                </div>
        @endforeach
    <br>
        <br>
        <br>
    @include('form_doc')
@endsection



