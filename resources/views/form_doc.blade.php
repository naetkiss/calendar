
    <div class="form" >
        <form id="form-ajax" enctype="multipart/form-data" method="post" action="{{ route('upload_file') }}">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <input type="file" id="file" multiple name="file">
            <input name="date" id="date" type= "date">
            <button type="submit">Загрузить</button>
        </form>
    </div>