<html lang="{{ config('app.locale') }}">
<head>
    <title>App Name - @yield('title')</title>
    @include('layout.header')
</head>
<body>

<div class="container">
    @yield('content')
</div>
</body>
</html>
