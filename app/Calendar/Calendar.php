<?php


namespace App\Calendar;

use DateTimeZone;
use function PHPSTORM_META\elementType;
use App\Calendar\Month;



class Calendar
{
    private $year;


    public function __construct($time = "Y", DateTimeZone $timezone = null)
    {
        $this->year = date("Y");
    }

    public function getYearNumber()
    {
        return $this->year;
    }


    public function getYear($year)
    {
        $this_year=[];
        for ($i = 1; $i <= 12; $i++) {
            $this_year[$i] = new Month($i, $year);
        }
        return $this_year;
    }
}
