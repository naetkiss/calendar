<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 21.06.17
 * Time: 16:24
 */

namespace App\Calendar;
use App\Calendar\Calendar;
use App\Dock;

class Day
{
    /**
     * Числовое представление дня
     *
     * @var int
     */
    private $dayNumber;

    /**
     * Булево представление для признака выходного дня
     *
     * @var bool  */
    private $isWeekend;
    private $file;

    public function __construct($dayNumber, $file = null)
    {
        $this->dayNumber = $dayNumber;
        $this->file = $file;
    }

    /**
     * @return integer
     */
    public function getDayNumber()
    {
        return $this->dayNumber;
    }

    /**
     * @return bool
     */
    public function getIsWeekend()
    {
        return $this->isWeekend;
    }

    /**
     * Присваивает значение true параметру
     *
     */
    public function thisIsWeekend()
    {
        $this->isWeekend = true;
    }

    public function thisIsNoWeekend()
    {
        $this->isWeekend = false;
    }

    /**
     * @return null
     */
    public function getFile()
    {
        return $this->file;
    }
}
