<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 23.06.17
 * Time: 19:19
 */

namespace App\Calendar;
use App\Dock;
use Psy\Exception\TypeErrorException;
use App\Calendar\Day;


class Month
{
    private $name = '';
    private $number;
    private $year;
    private $weeks =[];

    public function __construct(int $month, int $year)
    {
        $this->number = $month;
        $this->name = $this->getNameByNumber($month);
        $this->year = $year;
        $this->fillWeeks();
    }

    public function getNameByNumber($month)
    {
        switch ($month) {
            case ("01" ):
                $month = "Январь";
                break;

            case ("02"):
                $month = "Февраль";
                break;

            case ("03"):
                $month = "Март";
                break;

            case "04":
                $month = "Апрель";
                break;

            case "05":
                $month = "Май";
                break;

            case "06":
                $month = "Июнь";
                break;

            case "07":
                $month = "Июль";
                break;

            case "08":
                $month = "Авгут";
                break;

            case "09":
                $month = "Сентябрь";
                break;
            case "10":
                $month = "Октябрь";
                break;

            case "11":
                $month = "Ноябрь";
                break;
            case "12":
                $month = "Декабрь";
                break;
        }
        return $month;
    }

    private function fillWeeks()
    {
        $days_in_month = date('t', mktime(0, 0, 0, $this->number, 1, $this->year));
        $running_day_week = date('N', mktime(0, 0, 0, $this->number, 1, $this->year)) - 1;
        $end_day = 7 - ($days_in_month + $running_day_week) % 7;
        $n = $days_in_month - 1 + $running_day_week;
        $all_cells = $days_in_month + $running_day_week + $end_day ;
        $quantity_week = $all_cells/7;
        $array = [];

        for ($i = 0; $i < $running_day_week; $i++) {
            $array[$i] = null;
        }

        for ($i = $running_day_week; $i <= $n; $i++) {
            $number_day = $i - $running_day_week + 1;
            $file = null;
            $dock = Dock::where('date', "{$this->year}-{$this->number}-{$number_day}")->orderBy('id', 'DESC')->first();
            if ($dock) {
                $file = $dock->link;
            }
            $array[$i] = new Day($number_day, $file);
        }

        for ($i = $n + 1; $i <= $n + $end_day; $i++) {
            $array[$i] = null;
        }

        $weeks = array_chunk($array, 7);
        for ($i=0; $i<$quantity_week; $i++) {
            for ($n=0; $n<7; $n++) {
                if (is_object($weeks[$i][$n])) {
                    if (($n == 5) || ($n == 6)) {
                        $weeks[$i][$n]->thisIsWeekend();
                    } else {
                        $weeks[$i][$n]->thisIsNoWeekend();

                    }
                }
            }
        }
        $this->weeks = $weeks;
    }

    public function getWeek()
    {
        return $this->weeks;
    }

    public function getNumber()
    {

        return $this->number;
    }

    public function getName()
    {
        return $this->name;
    }
}
