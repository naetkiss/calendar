<?php
/**
 * Created by PhpStorm.
 * User: naet
 * Date: 27.06.17
 * Time: 19:39
 */

namespace App\Http\Controllers;


use App\Dock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UploadController extends Controller
{
    public function getForm()
    {
        return view('calendar');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function upload(Request $request)
    {
        foreach ($request->file() as $file) {
            $file->move(public_path('file'), time().'_'.$file->getClientOriginalName());
            $path = "file/". time().'_'.$file->getClientOriginalName();
        }
        $date = $request->input('date');
        $dock = new Dock([
            'link' => $path,
            'date' => $date
        ]);
        $dock->save();
        return view('uploaded');
    }

}