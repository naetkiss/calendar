<?php

namespace App\Http\Controllers;


use App\Calendar\Calendar;
use App\Calendar\Month;
use Illuminate\Http\Request;



class PageController extends Controller
{
    public function showName()
    {
         $calendar = new Calendar('2017');
         return view('calendar', compact('calendar'));
    }
}
