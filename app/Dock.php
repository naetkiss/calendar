<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dock extends Model
{
    protected $table = 'dock';
    protected $dates=['date'];
    public $timestamps = false;
    protected $fillable =['link','date'];
}
